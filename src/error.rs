quick_error! {
    #[derive(Debug)]
    pub enum Error {
        BLEDeviceNotFound {
            description("BLE Device not found while scanning")
        }
        NotMACAddress {
            description("Given argument is not proper MAC address")
        }
        BLEConnectError {
            description("BLE connection to server failed")
        }
        ConfigFileError {
            description("Configuration parsing error")
        }
    }
}
