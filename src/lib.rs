//#![deny(missing_docs)]

extern crate rand;
extern crate rumble;
#[macro_use]
extern crate log;
extern crate env_logger;
//use log::Level;
extern crate serde;
extern crate serde_json;
//#[macro_use] - I've implemended my own serialisation
#[macro_use]
extern crate serde_derive;
extern crate toml;

extern crate chrono;
use chrono::prelude::*;

#[macro_use(quick_error)]
extern crate quick_error;

extern crate regex;
use regex::Regex;

extern crate paho_mqtt as mqtt;
extern crate shellexpand;

use std::u64;

//use rand::{thread_rng, Rng};
use rumble::api::{BDAddr, Central, Peripheral, ValueNotification, UUID};
//use rumble::bluez::manager::Manager;
use rumble::bluez::adapter::Adapter;
use std::thread;
use std::time::Duration;

use serde::ser::{Serialize, SerializeStruct, Serializer};

use std::sync::mpsc::Sender;

pub mod cfg;
pub mod error;
pub mod handler;

//const APP_NAME: &'static str = env!("CARGO_PKG_NAME");

pub const MAC_ADDR_REGEXP: &'static str = "^([[:xdigit:]]{2}[:.-]?){5}[[:xdigit:]]{2}$";
pub const TEMPERATURE_UUID: [u8; 16] = [
    0x6d, 0x66, 0x70, 0x44, 0x73, 0x66, 0x62, 0x75, 0x66, 0x45, 0x76, 0x64, 0x55, 0xaa, 0x6c, 0x22,
];
pub const SOFTWARE_VERSION_UUID: u16 = 0x2a26;

pub type NotificationResult<T> = Result<T, error::Error>;

//226caa5564764566756266734470666d
#[derive(Debug)]
pub struct MijiaThermoHygromether {
    //pub id: &'a [u8;6],
    pub id: [u8; 6],
    pub model: String,
    pub sw_version: Option<String>,
    pub battery: Option<u8>,
    pub temperature: Option<f32>,
    pub humidity: Option<f32>,
}
impl MijiaThermoHygromether {
    pub fn new(id: &[u8; 6], model: &str) -> MijiaThermoHygromether {
        MijiaThermoHygromether {
            id: id.clone(),
            model: model.to_owned(),
            sw_version: None,
            battery: None,
            temperature: None,
            humidity: None,
        }
    }
    pub fn ble_mac(&self) -> String {
        format!(
            "{:x}:{:x}:{:x}:{:x}:{:x}:{:x}",
            self.id[5], self.id[4], self.id[3], self.id[2], self.id[1], self.id[0]
        )
    }
    pub fn into_influx_metric(&self) -> String {
        let tm: DateTime<Local> = Local::now();
        let sw_version: String = self.sw_version.clone().unwrap_or("".to_owned());
        format!(
            "mjhtv1,device_mac={},sw_version={} temperature={},humidity={},battery={} {}",
            self.ble_mac(),
            sw_version,
            self.temperature.unwrap(),
            self.humidity.unwrap_or(0.0),
            self.battery.unwrap_or(0),
            tm.timestamp() * 1_000_000_000
        )
    }
}
impl Serialize for MijiaThermoHygromether {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        //let ids = String::from_utf8(self.id.to_vec()).unwrap_or("?UNKNOWN?".to_string());
        let ids = format!(
            "{:x}:{:x}:{:x}:{:x}:{:x}:{:x}",
            self.id[5], self.id[4], self.id[3], self.id[2], self.id[1], self.id[0]
        );
        let mut state = serializer.serialize_struct("MijiaThermoHygromether", 6)?;
        state.serialize_field("model", &self.model)?;
        state.serialize_field("id", &ids)?;
        state.serialize_field("sw_version", &self.sw_version)?;
        state.serialize_field("battery", &self.battery)?;
        state.serialize_field("temperature", &self.temperature)?;
        state.serialize_field("humidity", &self.humidity)?;
        state.end()
    }
}
pub fn handler(
    s: MijiaThermoHygromether,
    ch: Sender<NotificationResult<MijiaThermoHygromether>>,
) -> impl Fn(ValueNotification) {
    move |v| {
        //Example sensor data: "T=24.1 H=48.0"
        let mut sensor = MijiaThermoHygromether::new(&s.id, "mjhtv1");
        sensor.battery = s.battery;
        sensor.sw_version = s.sw_version.clone();
        let data = String::from_utf8(v.value).unwrap_or(String::from("NO DATA"));
        info!("Raw data: {}", data);
        let re = Regex::new(r"^T=([+-]?[0-9]*[.]?[0-9]+) H=([+-]?[0-9]*[.]?[0-9]+).*").unwrap();
        if let Some(m) = re.captures(&data) {
            debug!("regex match len: {}", m.len());
            debug!("regex capture {:?}", m);
            if let Ok(temp) = m.get(1).unwrap().as_str().parse::<f32>() {
                sensor.temperature = Some(temp);
            };
            if let Ok(h) = m.get(2).unwrap().as_str().parse::<f32>() {
                sensor.humidity = Some(h);
            }
        } else {
            warn!("No sensible data received from sensor");
        }
        //match serde_json::to_string(&sensor) {
        //Ok(j) => {
        //println!("{}", j);
        match ch.send(Ok(sensor)) {
            Ok(_) => {}
            Err(e) => {
                debug!(
                    "Channel sending error - probably closed after first notification. ({})",
                    e
                );
            }
        }
        //process::exit(0);
        //return;
        //}
        //  Err(e) => warn!("Serialization error: {}", e),
        //}
    }
}
pub fn pool_mjhtv1_device(
    adapter: &Adapter,
    mac: &str,
    attempts: u32,
    channel: Sender<NotificationResult<MijiaThermoHygromether>>
) {
    // lets convert mac to BDAddr
    info!(
        "Processing reqeust for mac: {} with maximum {} scans",
        mac, attempts
    );
    let mut ble_id = [0u8; 6];
    //let mac_vec = mac.split(":");
    for (i, octet) in mac.split(":").enumerate() {
        let value = u64::from_str_radix(octet, 16).unwrap() as u8;
        ble_id[i] = value;
    }
    //Little Endian ..
    ble_id.reverse();
    let bdaddr = BDAddr { address: ble_id };
    //let manager = Manager::new().unwrap();
    // get the first bluetooth adapter
    //let adapters = match manager.adapters() {
    //        Ok(adapter) => adapter,
    /*        Err(e) => {
                info!("Erorr: {}", e);
                process::exit(-1);
            }
        };

        let mut adapter = match adapters.into_iter().nth(0) {
            Some(adapter) => adapter,
            None => {
                info!("No usable adapters found");
                process::exit(-1);
            }
        };
    */
    //   debug!("Calculated BDAddr {:?}", bdaddr);
    // reset the adapter -- clears out any errant state
    //   info!("Resetting adapter ..");
    // info!("Shutting down ..");
    /*    adapter =  match manager.down(&adapter) {
            Ok(a) => a,
            Err(e) => {
                warn!("Shutting down error !");
                process::exit(-1);
            }

        };
        info!("Bringing up ..");
        adapter = manager.up(&adapter).unwrap();
    */
    info!("Connecting to adapter ..");
    // connect to the adapter
    let central = adapter.connect().unwrap();
    info!("Scanning ..");
    let mut loop_cnt = 0;
    let mj = loop {
        debug!("Scan attempt no {}", loop_cnt + 1);
        if loop_cnt > attempts {
            warn!("Unable to find device {}", mac);
            let _sender = channel.clone().send(Err(error::Error::BLEDeviceNotFound));
            return;
        }
        // start scanning for devices
        central.start_scan().unwrap();
        // instead of waiting, you can use central.on_event to be notified of
        // new devices
        thread::sleep(Duration::from_secs(2));

        info!("Searching MJ_HT_V1 with mac: {}", mac);
        for d in central.peripherals().into_iter() {
            let p = d.properties();
            debug!(
                "Found server: Name: {:?}, BDAddr: {:?}",
                p.local_name,
                d.address()
            );
        }
        match central
            .peripherals()
            .into_iter()
            .find(|p| p.address() == bdaddr)
        {
            Some(mj) => {
                break mj;
            }
            None => {
                info!("No device found :-( ..");
                loop_cnt = loop_cnt + 1;
                thread::sleep(Duration::from_millis(300));
                continue;
            }
        };
    };
    let id = mj.address().address.clone();
    let mut sensor = MijiaThermoHygromether::new(&id, "mjhtv1");
    info!("Found one ! MAC: {}", mj.address());
    //for ble in central.peripherals().into_iter() {
    //   info!("Peripheral address: {} Name: {}", ble.address(),ble.properties().local_name.unwrap_or(String::from("")));
    // }
    //
    info!("Connecting to MJ_HT_V1 ..");
    //let mut connect_attempts: u32 = 0;
    //  loop {
    //    if connect_attempts > 2 {
    //      warn!("Two connection attepts failed !");
    //    return
    // }
    match mj.connect() {
        Ok(_) => {
            info!("Connected ! ..");
            //    break;
        }
        Err(e) => {
            warn!("Unable to connect !:{}", e);
            let msg_channel = channel.clone();
            // this lets us to bypass waiting for thread timeout
            let _sender = msg_channel.send(Err(error::Error::BLEConnectError));
            return
            //connect_attempts = connect_attempts +1;
            //process::exit(-1);
            //   }
        }
    }
    // discover characteristics
    info!("Discovering characteristics ..");
    match mj.discover_characteristics() {
        Ok(_) => info!("Characteristics discovered !"),
        Err(e) => {
            info!("Error discovering characteristic: {}", e);
            let _sender = channel.send(Err(error::Error::BLEConnectError));
            return
        }
    }

    info!("Finding characteristic..");
    let chars = mj.characteristics();
    //for c in chars.iter() {
    //  info!("Characteristic: {} \n\tProperties: {:?}", c, c.properties);
    //  info!("Characteristic: {}\n'tType: {}", c);
    //  if c.properties.intersects(CharPropFlags::READ) {
    // let value = mj.read_by_type(&c, c.uuid).unwrap();
    // info!("\tValues: {:?}", value);
    //}
    // }
    // find the characteristic we want
    let temp_uid = UUID::B128(TEMPERATURE_UUID);
    let sw_ver = chars
        .iter()
        .find(|c| c.uuid == UUID::B16(SOFTWARE_VERSION_UUID))
        .unwrap();
    //};
    // read_by_type return whole packet response and there are following data by byte position:
    // 0 - Method usually 9 - Read by Type Response
    // 1 - packet length
    // 2,3 (u16) - handle
    // 4 upto packet length - returned data
    // **** this code works **** //
    let sw_ver_raw = mj.read_by_type(&sw_ver, UUID::B16(0x2a26)).unwrap();
    let sw_ver_len = sw_ver_raw.len();
    let sw_raw: Vec<u8> = sw_ver_raw[4..sw_ver_len].to_vec();
    let sw_ver_value: String = String::from_utf8(sw_raw).unwrap_or("Decoding error".to_string());
    let sw_version = &sw_ver_value;
    sensor.sw_version = Some(sw_version.to_owned());
    let battery = chars.iter().find(|c| c.value_handle == 0x18).unwrap();
    //info!("Battery characteristc: {}", battery);
    let battery_level = mj.read_by_type(&battery, UUID::B16(0x2a19)).unwrap();
    let battery_value = battery_level[battery_level.len() - 1];
    sensor.battery = Some(battery_value);
    //info!("Battery: {}%", battery_level[battery_level.len() - 1]);
    //info!("SW VERSION: {:?}", sw_ver_value);

    // *** Litle Endian !! //
    //info!("Searching for temp uid {}", temp_uid);
    let temp_characteristic = match chars.iter().find(|c| c.uuid == temp_uid) {
        Some(t) => t,
        None => {
            info!("Unable to find temperture characteristic ..");
            let _sender = channel.send(Err(error::Error::BLEConnectError));
            return
        }
    };
    //println!("Sensor status: {:?}", sensor);
    //let handle_fn = | v: ValueNotification | {
    //info!("Temp characteristic: {}", temp_characteristic); //println!("Subscribing..");
    //mj.on_notification(Box::new(handle_temp));
    let notification_channel = channel.clone();
    mj.on_notification(Box::new(handler(sensor, notification_channel)));
    match mj.subscribe(&temp_characteristic) {
        Ok(_) => {}
        Err(e) => {
            info!("Unable to subscribe to temperature notification: {}", e);
            let _sender = channel.send(Err(error::Error::BLEConnectError));
            return
        }
    }
    //let temp_val = mj.request(&sensor_data, &[0x1, 0x0]).unwrap();
    thread::sleep(Duration::from_millis(1000));
    //info!("Temperature: {:?}", temp_val);
    info!("Disconecting ..");
    mj.disconnect().unwrap();
    let _sender = channel.clone().send(Err(error::Error::BLEConnectError));
}

pub fn check_and_sanitize_mac(mac: &str) -> Result<String, error::Error> {
    let mac_re =
        regex::Regex::new(MAC_ADDR_REGEXP).expect("Regexp parsing error. This shouldn't happend.");
    match mac_re.is_match(mac) {
        true => {
            let mut int_mac: String = mac.clone().into();
            let mut m: String = String::from("");
            for s in &["-", ":", "."] {
                m = int_mac.replace(s, "").clone().into();
                int_mac = m.clone();
            }
            m = m.to_lowercase();
            Ok(format!(
                "{}:{}:{}:{}:{}:{}",
                &m[0..2],
                &m[2..4],
                &m[4..6],
                &m[6..8],
                &m[8..10],
                &m[10..12]
            ))
        }
        false => Err(error::Error::NotMACAddress),
    }
    // unimplemented!()
}
