use super::error;
use std::fs::File;
use std::io::Read;
use std::path::Path;

pub const CFG_NAME: &'static str = "xiaomi-sensor.conf";

#[derive(Serialize, Deserialize, Clone)]
pub struct Config {
    pub mqtt_server: String,
    pub mqtt_port: u16,
    pub mqtt_protocol: String,
    pub mqtt_topic: String,
}

pub fn get_config(cfgpath: Option<&str>) -> Result<Config, error::Error> {
    if let Some(given_path) = cfgpath {
        info!("Processing cfg file from path given from command line");
        let config_file = get_config_content(given_path);
        if config_file.is_empty() {
            Err(error::Error::ConfigFileError)
        } else {
            let cfg: Config = toml::from_str(&config_file).expect("Config parsing error");
            Ok(cfg)
        }
    } else {
        info!("Processing default config paths");
        let local_path: String = shellexpand::full("~/.config/xiaomi-sensor")
            .unwrap()
            .into_owned();
        let cfg_search_path = vec!["/etc/", &local_path, "."];
        for p in cfg_search_path {
            let fullpath = Path::new(p).join(CFG_NAME).to_str().unwrap().to_owned();
            debug!("Searching for cfg in {}", fullpath);
            let config_file = get_config_content(&fullpath);
            match config_file.is_empty() {
                true => continue,
                false => {
                    let cfg: Config = toml::from_str(&config_file).expect("Config parsing error");
                    return Ok(cfg);
                }
            };
        }

        Err(error::Error::ConfigFileError)
    }
}
pub fn get_config_content(cfg_search_path: &str) -> String {
    let mut config_file: String = String::from("");
    //for p in cfg_search_path {
    // println!("Searching config {}/{}", p, CFG_NAME);
    match File::open(Path::new(cfg_search_path)) {
        Ok(mut f) => {
            info!("Found config {:?}", Path::new(cfg_search_path));
            let _ = f.read_to_string(&mut config_file);
            // this prevent to read multiple files
            //break;
        }
        Err(e) => {
            debug!("{:?}: {}", Path::new(cfg_search_path), e);
            //continue;
        }
    }
    //}
    config_file
}
