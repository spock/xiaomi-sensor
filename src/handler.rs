//use rumqtt::MqttCallback;
//use rumqtt::{MqttClient, MqttOptions, QoS};
//

// mqtt handler
pub trait SensorHandler {
    fn handle(&self, msg: Vec<String>);
}

#[derive(Debug)]
pub struct MQTTHandler {
    pub mqtt_server: String,
    pub mqtt_port: u16,
    pub mqtt_protocol: String,
    pub mqtt_topic: String,
}
impl MQTTHandler {
    pub fn new(server: &str, port: u16, protocol: &str, topic: &str) -> MQTTHandler {
        MQTTHandler {
            mqtt_server: server.to_owned(),
            mqtt_port: port,
            mqtt_protocol: protocol.to_owned(),
            mqtt_topic: topic.to_owned(),
        }
    }
}

impl SensorHandler for MQTTHandler {
    //    fn handle(&self,msg: &str) {
    //        let topic = format!("{}/{}",self.mqtt_topic, "mjhtv1");
    //        info!("handle(): Received message to send on channel {}", &topic);
    //        info!("handle() MQTT details: {:?}", self);
    //		let client_options = MqttOptions::new()
    //						  .set_keep_alive(5)
    //						  .set_reconnect(3)
    //						  .set_client_id(super::APP_NAME)
    //						  .set_broker(&format!("{}:{}",self.mqtt_server,self.mqtt_port));
    //		let mq_cbs = MqttCallback::new().on_message(|x| warn!("Received MQTT message. Shouldn't happend. Message: {:?}",x));
    //		let mut request = MqttClient::start(client_options, Some(mq_cbs)).expect("Coudn't start mqtt");
    //		//let mut request = MqttClient::start(client_options, None).unwrap();
    //        info!("Sending message: {}", msg);
    //		request.publish(&topic, QoS::Level2, msg.to_owned().into_bytes()).unwrap();
    //        request.disconnect();
    //        request.shutdown();
    //
    //    }
    fn handle(&self, data: Vec<String>) {
        let topic = format!("{}/{}", self.mqtt_topic, "mjhtv1");
        debug!("handle(): Received message to send on channel {}", &topic);
        debug!("handle() MQTT details: {:?}", self);
        let host = format!(
            "{}://{}:{}",
            self.mqtt_protocol, self.mqtt_server, self.mqtt_port
        );
        if let Ok(cli) = mqtt::Client::new(host) {
        if let Err(e) = cli.connect(None) {
            println!("Unable to connect: {:?}", e);
        }
        // Create a message and publish it
        for msg in data {
            let t = topic.clone();
            let msg = mqtt::MessageBuilder::new()
                .topic(t)
                .payload(msg)
                .qos(1)
                .finalize();
            if let Err(e) = cli.publish(msg) {
                println!("Error sending message: {:?}", e);
            }
        }
        // Disconnect from the broker
        match cli.disconnect(None) {
            Ok(_) => {},
            Err(e) => warn!("Error proper disconecting from MQTT broker: {}",e)
        }
        } else {
            warn!("MQTT Client creation error - not sending message");
        }
    }
}
