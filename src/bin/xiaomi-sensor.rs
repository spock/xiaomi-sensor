extern crate clap;
#[macro_use]
extern crate log;
extern crate env_logger;
extern crate xiaomi_sensor;
use xiaomi_sensor::handler::{MQTTHandler, SensorHandler};

use clap::App;
use rumble::bluez::manager::Manager;

use std::process;
use std::sync::mpsc;
use std::sync::mpsc::{Receiver, Sender};
use std::thread;
use std::time::Duration;

const APP_VERSION: &'static str = env!("CARGO_PKG_VERSION");
const APP_NAME: &'static str = env!("CARGO_PKG_NAME");
const APP_AUTHOR: &'static str = env!("CARGO_PKG_AUTHORS");

//use rumble::bluez::adapter::Adapter;
// factor for how long wait for response from device
// it should be sum of scanning time, discovering characteristic, and response from device
// value calculated by experience
//const THREAD_TIMEOUT_FACTOR: u64 = 5;

fn main() {
    env_logger::init();
    let app = App::new("xiaomi-sensor")
        .version(APP_VERSION)
        .author(APP_AUTHOR)
        .about("xiaomi-sensor is BLE (Bluetooth Low Energy) device data pooler")
        .subcommand(
            clap::SubCommand::with_name("mjhtv1").subcommand(
                clap::SubCommand::with_name("query")
                    .about("Query Xiaomi MJ_HT_V1 thermometer/higrometer for data")
                    .arg(
                        clap::Arg::with_name("attempts")
                            .short("a")
                            .long("attempts")
                            .help("Number of scanning attempts to find peripheral. (Default: 3)")
                            .takes_value(true),
                    )
                    .arg(
                        clap::Arg::with_name("config")
                            .short("c")
                            .long("config")
                            .help("Configuration file for export providers")
                            .takes_value(true),
                    )
                    .arg(
                        clap::Arg::with_name("format")
                            .short("f")
                            .long("format")
                            .help("Output format. Supported: [influx]")
                            .takes_value(true),
                    )
                    .arg(
                        clap::Arg::with_name("bletimeout")
                            .short("b")
                            .long("bletimeout")
                            .help("How long wait untile considering ble connection timeout")
                            .takes_value(true),
                    )
                    .arg(clap::Arg::with_name("macaddr").required(true).index(1)),
            )
            .subcommand(
                clap::SubCommand::with_name("pool")
                    .about("Daemon mode with pool for Xiaomi MJ_HT_V1 thermometer/higrometer data. Default every 1 minute")
                    .arg(
                        clap::Arg::with_name("attempts")
                            .short("a")
                            .long("attempts")
                            .help("Number of scanning attempts to find peripheral. (Default: 3)")
                            .takes_value(true),
                    )
                    .arg(
                        clap::Arg::with_name("bletimeout")
                            .short("b")
                            .long("bletimeout")
                            .help("How long wait untile considering ble connection timeout")
                            .takes_value(true),
                    )
                    .arg(
                        clap::Arg::with_name("config")
                            .short("c")
                            .long("config")
                            .help("Configuration file for export providers")
                            .takes_value(true),
                    )
                    .arg(
                        clap::Arg::with_name("format")
                            .short("f")
                            .long("format")
                            .help("Output format. Supported: [influx]")
                            .takes_value(true),
                    )
                    .arg(
                         clap::Arg::with_name("time")
                        .short("t")
                        .long("time")
                        .help("How long wait between next scan in seconds")
                        )
                    .arg(clap::Arg::with_name("macaddr").required(true).index(1)),
                       )
        )
        .get_matches();
    // match app.subcommand_name() {
    match app.subcommand() {
        ("mjhtv1", Some(args)) => handle_mjhtv1(&args),
        (&_, None) | (&_, Some(_)) => {
            println!(
                "Missing command. {} --help to see available options",
                APP_NAME
            );
        }
    }
}

fn handle_mjhtv1(subcmd: &clap::ArgMatches) {
    match subcmd.subcommand() {
        ("pool", Some(args)) => {
            info!("Starting xiaomi-sensor {} in pool mode for mjhtv1 sensor", APP_VERSION);
            let attempts = match args.value_of("attempts").unwrap_or("3").parse::<u32>() {
                Ok(v) => v,
                Err(e) => {
                    println!("Wrong attempts argument value - must be a number ({})", e);
                    process::exit(-1);
                }
            };
            let format = args.value_of("format").unwrap_or("influx").to_owned();
            info!("Selected output format: {}",format);
            let sleep_period = match args.value_of("time").unwrap_or("60").parse::<u64>() {
                Ok(v) => v,
                Err(_) => {
                    println!("Wrong value for sleep time between pools");
                    process::exit(-1);
                }
            };
            let ble_timeout = match args.value_of("bletimeout").unwrap_or("5").parse::<u64>() {
                Ok(v) => v,
                Err(_) => {
                    println!("Wrong value for ble timeout");
                    process::exit(-1);
                }
            };
            let cfg = match xiaomi_sensor::cfg::get_config(args.value_of("config")) {
                Ok(c) => c,
                Err(e) => {
                    println!("Error getting config: {}", e);
                    process::exit(-3);
                }
            };

            let devices_mac: Vec<&str> = args.value_of("macaddr").unwrap().split(",").collect();
            let adapter = prepare_ble_interface();
            loop {
            for device in &devices_mac {
                let (tx, rx): (
                    Sender<
                        xiaomi_sensor::NotificationResult<xiaomi_sensor::MijiaThermoHygromether>,
                    >,
                    Receiver<
                        xiaomi_sensor::NotificationResult<xiaomi_sensor::MijiaThermoHygromether>,
                    >,
                ) = mpsc::channel();
                //let config = cfg.clone();
                let mqtt_handler = MQTTHandler::new(
                    &cfg.mqtt_server,
                    cfg.mqtt_port,
                    &cfg.mqtt_protocol,
                    &cfg.mqtt_topic,
                );
                let move_format = format.clone();
                let ch = thread::spawn(move || {
                    mjhtv1_processor(
                        rx,
                        attempts as u64 * ble_timeout,
                        &mqtt_handler,
                        &move_format,
                    );
                });
                xiaomi_sensor::pool_mjhtv1_device(&adapter, &device, attempts, tx.clone());
                ch.join().expect("oops! the child thread panicked");
            }
            info!("Finished pool tasks. Sleeping for {} seconds.", sleep_period);
            thread::sleep(Duration::from_millis(sleep_period * 1000));

        }

            
        },
        ("query", Some(args)) => {
            let attempts = match args.value_of("attempts").unwrap_or("3").parse::<u32>() {
                Ok(v) => v,
                Err(e) => {
                    println!("Wrong attempts argument value - must be a number ({})", e);
                    process::exit(-1);
                }
            };
            let format = args.value_of("format").unwrap_or("influx").to_owned();
            let ble_timeout = match args.value_of("bletimeout").unwrap_or("5").parse::<u64>() {
                Ok(v) => v,
                Err(_) => {
                    println!("Wrong value for ble timeout");
                    process::exit(-1);
                }
            };
            info!("Selected output format: {}",format);
            let cfg = match xiaomi_sensor::cfg::get_config(args.value_of("config")) {
                Ok(c) => c,
                Err(e) => {
                    println!("Error getting config: {}", e);
                    process::exit(-3);
                }
            };

            let devices_mac: Vec<&str> = args.value_of("macaddr").unwrap().split(",").collect();
            //println!("Searching devices {:?}", devices_mac);
            for device in devices_mac {
                let (tx, rx): (
                    Sender<
                        xiaomi_sensor::NotificationResult<xiaomi_sensor::MijiaThermoHygromether>,
                    >,
                    Receiver<
                        xiaomi_sensor::NotificationResult<xiaomi_sensor::MijiaThermoHygromether>,
                    >,
                ) = mpsc::channel();
                //let config = cfg.clone();
                let mqtt_handler = MQTTHandler::new(
                    &cfg.mqtt_server,
                    cfg.mqtt_port,
                    &cfg.mqtt_protocol,
                    &cfg.mqtt_topic,
                );
                let move_format = format.clone();
                let ch = thread::spawn(move || {
                    mjhtv1_processor(
                        rx,
                        attempts as u64 * ble_timeout,
                        &mqtt_handler,
                        &move_format,
                    );
                });
                let adapter = prepare_ble_interface();
                xiaomi_sensor::pool_mjhtv1_device(&adapter, &device, attempts, tx.clone());
                ch.join().expect("oops! the child thread panicked");
            }
        }
        (&_, None) | (&_, Some(_)) => {
            println!("Unknown subcommand for mjhtv1 menu. Try mjhtv1 --help")
        }
    }
}
// The only way to get only one notification I can think of with my rust knowledge

//fn processor(rx: Receiver<String>, timeout_secs: u64) {
fn mjhtv1_processor(
    rx: Receiver<xiaomi_sensor::NotificationResult<xiaomi_sensor::MijiaThermoHygromether>>,
    timeout_secs: u64,
    handler: &SensorHandler,
    format: &str,
) {
    let timeout = Duration::new(timeout_secs, 0);
    info!(
        "Thread for notification with {} seconds timeout started.",
        timeout_secs
    );
    if let Ok(i) = rx.recv_timeout(timeout) {
        match i {
            Ok(msg) => {
                match format {
                    "influx" => {
                        let mut data: Vec<String> = Vec::new();
                        data.push(msg.into_influx_metric());
                        for m in &data {
                            println!("{}", m);
                        }
                        handler.handle(data);
                    },
                    "json" =>{
                        match serde_json::to_string(&msg) {
                            Ok(j) => {
                                println!("{}",j);
                                handler.handle(vec![j]);
                            },
                            Err(e) => {
                                println!("JSON serialzation error: {}", e);
                            }

                    }
                    },
                    _ => {
                        println!("Unsupported output format!: call --help for selected subcommand to get more info");
                    }
                }
                info!("Thread handler finising ..");
            }
            Err(e) => warn!("Notification error: {}", e),
        }
    } else {
        warn!("Notification thread timeout !")
    }
}
fn prepare_ble_interface() -> rumble::bluez::adapter::Adapter {
            let manager = Manager::new().unwrap();
            let adapters = match manager.adapters() {
                Ok(adapter) => adapter,
                Err(e) => {
                    info!("Erorr: {}", e);
                    process::exit(-1);
                }
            };

            let mut adapter = match adapters.into_iter().nth(0) {
                Some(adapter) => adapter,
                None => {
                    info!("No usable adapters found");
                    process::exit(-1);
                }
            };
            // reset the adapter -- clears out any errant state
            info!("Resetting adapter ..");
            info!("Shutting down ..");
            adapter = match manager.down(&adapter) {
                Ok(a) => a,
                Err(e) => {
                    warn!("Shutting down error !: {}", e);
                    process::exit(-1);
                }
            };
            //let mut ready = false;
            info!("Bringing up ..");
            loop {
                //if ready {
                 //   break;
                //}
                match manager.up(&adapter) {
                    Ok(v) => {
                        return v;
                    },
                    Err(e) => {
                        warn!("Unable to bring adapter up: {}", e);
                        thread::sleep(Duration::from_millis(1000));

                    }
                }

        };
        
}
